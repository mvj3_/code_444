#需要注意的是，Timeout::Error 不是 StandardError 的子类, 而是继承至 Interrupt class ，所以捕获的时候，需要格外注意
require 'net/pop3'  
begin  
  Net::POP3.auth_only(@server, @port, @username, @password)  
rescue => e  
  write_error_to_logfile(e)  
  do_something_sensible  
rescue Timeout::Error => e  
  write_error_to_logfile(e)  
  do_something_sensible_for_timeout  
end 

#如果您知道对方的服务器会比较慢的响应，或者你知道网络状态不好，你可以单独设置这个 TimeOut 的时间，代码如下
require 'timeout'  
#...  
#...  
begin  
  timeout(60) do  
     resp, body=3Dh.get('/index.html')  
     puts body  
  end  
rescue TimeoutError  
       puts "Timed Out"  
end

# 或者这样：
http = Net::HTTP.new(url.host, url.port)  
http.read_timeout=time_out